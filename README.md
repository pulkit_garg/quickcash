## Quick Cash

This project is a clone of the repo created for 3130 software enginnering course at Dalhousie University in Winter 2020. The group created an Android application that allows one to find small paid work in the local area.  

### Features

Currently the follwing features have been implemented in the application:

1. The local area of an app user can be identified and is updated dynamically. 
2. Types of small tasks on the app such as repairing a computer, mowing the lawn, walking a dog etc. have been identified and can be added by a worker. 
3. Two types of users are currently allowed: Employee and Employer. They could link their PayPal account with the app for transactions. 
4. Both Employer and Employee can create an account in the app and also create respective profiles. 
5. An Employer can submit a task with necessary details such as date, expected duration, place, urgency, price. 
6. An Employee can search for tasks with various parameters and preferences. 
7. An Employee can apply for a task by uploading their resume, along with their profile. 
8. An Employee gets a notification on their device when an Employer accepts their application. 
9. Profile for an Employee also contains a rating section which consists the average rating given by all the Employers that they worked with. 

### Future Plans

The following features could be added in the application in the future:

1. A chatting functionality to allow communication between an Employer and Employee.
2. Allow the Employer to add images when they submit a task. 
3. Allow the users to add an image for their profile picture. 
4. Implement the functionality to send notification to an Employer once an Employee is registered in the proximity of Employer's location. 
5. Make the UI more impressive. 

### Relevant Technology Used

The following technologies and resources have been used to develop the application:
1. Firebase has been used to setup the backend database. 
2. Google Maps API has been used to integrate the location functionality. 
3. PayPal API has been used for Paypal Integration.
4. Android Studio was used by the team for programming along with a suitable virtual device.
5. Java programming language has been used to develop the application.  
